// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueCircleSlider from 'vue-circle-slider'
//import * as VueMenu from '@hscmap/vue-menu'

Vue.config.productionTip = false
Vue.config.ribbit = 'test';
Vue.use(VueCircleSlider);
//Vue.use(VueMenu);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
