import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Window from '@/components/Window'
import Rest from '@/components/Rest'

Vue.use(Router)


export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/window',
      name: 'Window',
      component: Window
    },
    {
        path: '/rest',
        name: 'Rest',
        component: Rest
    }
  ]
})
